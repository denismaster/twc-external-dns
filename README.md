# twc-external-dns

⚠️ This software is experimental and NOT FIT FOR PRODUCTION USE!

## Features
- Get DNS records from TimeWeb Cloud
- Create new DNS records in TimeWeb Cloud

## TODO Features
- Update records in TimeWeb Cloud
- Remove records from TimeWeb Cloud

## Install

Create a secret containing a TIMEWEB_CLOUD token:
```bash
kubectl --kubeconfig ..\test.kubeconfig create secret generic twc-credentials --from-literal=api-token='<TWC_TOKEN>'
```

Install external DNS using Helm:
```bash
helm upgrade --install external-dns-twc bitnami/external-dns -f ./values.yaml
```

Example `values.yaml`:
```yaml
provider: webhook

extraArgs:
  webhook-provider-url: http://localhost:8080
  txt-prefix: twc-

sidecars:
  - name: twc-webhook
    # Change to recent build hash
    image: registry.gitlab.com/denismaster/twc-external-dns:45741374
    ports:
      - containerPort: 8080
        name: http
    env:
      - name: TimeWebCloud__Token
        valueFrom:
          secretKeyRef:
            name: twc-credentials
            key: api-token
      - name: TimeWebCloud__ApiUrl
        value: "https://api.timeweb.cloud/api/v1"
```
