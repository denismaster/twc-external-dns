FROM mcr.microsoft.com/dotnet/aspnet:8.0
COPY src/TimeWebCloud.ExternalDns/bin/Release/net8.0/publish/ app/
WORKDIR /app
ENTRYPOINT ["dotnet", "TimeWebCloud.ExternalDns.dll"]
