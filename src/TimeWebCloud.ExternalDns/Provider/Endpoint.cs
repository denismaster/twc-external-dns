﻿namespace TimeWebCloud.ExternalDns.Provider;

public class Endpoint
{
    public string DnsName { get; set; } = string.Empty;
    public List<string> Targets { get; set; } = new();
    public string RecordType { get; set; } = string.Empty;
    public string SetIdentifier { get; set; } = string.Empty;
    public long RecordTTL { get; set; }
    public Dictionary<string, string> Labels { get; set; } = new();
    public List<ProviderSpecificProperty> ProviderSpecific { get; set; } = new();
}
