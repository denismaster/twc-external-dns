﻿namespace TimeWebCloud.ExternalDns.Provider;

public static class RecordTypes
{
    public const string A = nameof(A);
    public const string TXT = nameof(TXT);
}
