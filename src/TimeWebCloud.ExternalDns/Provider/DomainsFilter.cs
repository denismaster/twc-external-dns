﻿namespace TimeWebCloud.ExternalDns.Provider;

public class DomainsFilter
{
    public List<string> Filters { get; set; } = new();
}
