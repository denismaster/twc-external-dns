﻿namespace TimeWebCloud.ExternalDns.Provider;

public class ProviderSpecificProperty
{
    public string Name { get; set; } = string.Empty;
    public string Value { get; set; } = string.Empty;
}
