﻿using System.Text.Json;

namespace TimeWebCloud.ExternalDns.Provider;

public class ExternalDnsJsonResult<T>: IResult
{
    private const string ContentType = "application/external.dns.webhook+json;version=1";
    private readonly T _result;

    public ExternalDnsJsonResult(T result)
    {
        _result = result;
    }

    public async Task ExecuteAsync(HttpContext httpContext)
    {
        await httpContext.Response.WriteAsJsonAsync(
            _result,
            new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            },
            contentType: ContentType
        );
    }
}

static class ResultExtensions
{
    public static IResult ExternalDnsJson<T>(this IResultExtensions _, T result)
        => new ExternalDnsJsonResult<T>(result);
}
