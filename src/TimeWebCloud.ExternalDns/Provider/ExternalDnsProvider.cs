﻿using System.Net;
using System.Text.RegularExpressions;
using Refit;
using TimeWebCloud.ExternalDns.Api;

namespace TimeWebCloud.ExternalDns.Provider;

public class ExternalDnsProvider
{
    private readonly ITimeWebCloudApi _timeWebCloudApi;
    private readonly ILogger<ExternalDnsProvider> _logger;

    public ExternalDnsProvider(
        ITimeWebCloudApi timeWebCloudApi,
        ILogger<ExternalDnsProvider> logger)
    {
        _timeWebCloudApi = timeWebCloudApi;
        _logger = logger;
    }

    public async Task<DomainsFilter> GetDomainsFilter()
    {
        var timeWebDomains = await GetDomainsInTimeWeb();

        var domainFqdns = timeWebDomains.Domains
            .Select(dto => dto.Fqdn)
            .ToList();

        return new DomainsFilter { Filters = domainFqdns };
    }

    public async Task<List<Endpoint>> GetCurrentRecords()
    {
        var dnsZones = await GetDnsZones();

        var endpoints = new List<Endpoint>();

        foreach (string dnsZone in dnsZones)
        {
            var dnsRecords = await _timeWebCloudApi.GetDnsRecordsForFqdn(dnsZone);

            var endpointForZone = dnsRecords.DnsRecords
                .Select(timeWebRecord => new Endpoint
                {
                    RecordType = timeWebRecord.Type,
                    DnsName = string.IsNullOrWhiteSpace(timeWebRecord.Data.SubDomain)
                        ? dnsZone
                        : timeWebRecord.Type == "TXT" && timeWebRecord.Data.SubDomain.Contains(dnsZone)
                            ? timeWebRecord.Data.SubDomain
                            : $"{timeWebRecord.Data.SubDomain}.{dnsZone}",
                    Targets = new List<string> { timeWebRecord.Data.Value },
                    ProviderSpecific = new List<ProviderSpecificProperty>
                    {
                        new() { Name = "TimeWebId", Value = timeWebRecord.Id.ToString() },
                        new() { Name = "TimeWebZone", Value = dnsZone }
                    }
                });

            endpoints.AddRange(endpointForZone);
        }

        return endpoints;
    }

    public Task<List<Endpoint>> AdjustEndpoints(List<Endpoint> endpoints)
    {
        // We do not need to adjust them for now
        return Task.FromResult(endpoints);
    }

    public async Task ApplyChanges(Changes changes)
    {
        // Should not be null, but ExternalDNS can send a bullshit to us
        changes.Create ??= new List<Endpoint>();
        changes.UpdateOld ??= new List<Endpoint>();
        changes.UpdateNew ??= new List<Endpoint>();
        changes.Delete ??= new List<Endpoint>();

        var dnsZones = await GetDnsZones();

        var endpointsToDelete = changes.Delete
            .Union(changes.UpdateOld)
            .ToList();

        var endpointsToCreate = changes.Create
            .Union(changes.UpdateNew)
            .ToList();

        foreach (var endpoint in endpointsToDelete)
        {
            string matchedDnsZone = GetDnsZoneForRecord(
                endpoint.DnsName,
                endpoint.RecordType,
                dnsZones
            );

            string subDomain = GetSubdomainForRecord(
                endpoint.DnsName,
                matchedDnsZone,
                endpoint.RecordType,
                false
            );

            _logger.LogInformation(
                "Trying to delete record with Type={recordType} with SubDomain={subDomain} in Zone={zone}",
                endpoint.RecordType,
                subDomain,
                matchedDnsZone
            );

            var existingRecordsInZone = await _timeWebCloudApi.GetDnsRecordsForFqdn(matchedDnsZone);

            var existingDnsRecordInfo = existingRecordsInZone.DnsRecords
                .Where(record => record.Type == endpoint.RecordType)
                .FirstOrDefault(record => record.Data.SubDomain == subDomain);

            if (existingDnsRecordInfo is null)
            {
                throw new Exception();
            }

            await _timeWebCloudApi.DeleteDnsRecord(
                matchedDnsZone,
                existingDnsRecordInfo.Id
            );
        }

        foreach (var endpoint in endpointsToCreate)
        {
            string matchedDnsZone = GetDnsZoneForRecord(
                endpoint.DnsName,
                endpoint.RecordType,
                dnsZones
            );

            if (endpoint.DnsName != matchedDnsZone
                && endpoint.RecordType == RecordTypes.A
                && endpoint.DnsName.Contains(matchedDnsZone)
               )
            {
                string subDomainNameToCreate = endpoint.DnsName
                    .Replace($".{matchedDnsZone}", "");

                try
                {
                    await _timeWebCloudApi.CreateSubDomain(
                        matchedDnsZone,
                        subDomainNameToCreate
                    );
                }
                catch (ApiException apiException)
                {
                    if (apiException.StatusCode == HttpStatusCode.Conflict)
                    {
                        _logger.LogWarning("Unable to create sub-domain {subDomain}", subDomainNameToCreate);
                    }
                    else
                    {
                        throw;
                    }
                }

                matchedDnsZone = endpoint.DnsName;
            }

            string subDomain = GetSubdomainForRecord(
                endpoint.DnsName,
                matchedDnsZone,
                endpoint.RecordType,
                true
            );

            foreach (string endpointTarget in endpoint.Targets)
            {
                try
                {
                    await _timeWebCloudApi.CreateDnsRecord(
                        matchedDnsZone,
                        new TwcCreateDnsRecordRequest
                        {
                            Type = endpoint.RecordType,
                            SubDomain = subDomain,
                            Value = endpointTarget.Replace("\"", "")
                        }
                    );
                }
                catch (ApiException apiException)
                {
                    if (apiException.StatusCode == HttpStatusCode.Conflict)
                    {
                        _logger.LogWarning(
                            apiException,
                            "Unable to create dns record of type {type} for {fqdn} due to conflict",
                            endpoint.RecordType,
                            endpoint.DnsName
                        );
                    }
                    else
                    {
                        _logger.LogError(
                            apiException,
                            "Unable to create dns record of type {type} for {fqdn}",
                            endpoint.RecordType,
                            endpoint.DnsName
                        );
                        throw;
                    }
                }
            }
        }
    }

    private async Task<TwcGetDomainsResponse> GetDomainsInTimeWeb()
    {
        var timeWebDomains = await _timeWebCloudApi.GetAllDomains();

        // We remove technical domains for now
        timeWebDomains.Domains = timeWebDomains.Domains
            .Where(timeWebDomain => !timeWebDomain.Fqdn.Contains(".tw1.ru"))
            .ToList();

        return timeWebDomains;
    }

    private async Task<List<string>> GetDnsZones()
    {
        var timeWebDomains = await GetDomainsInTimeWeb();

        var dnsZones = timeWebDomains.Domains
            .SelectMany(x =>
            {
                var subDomainsFqdns = x.SubDomains
                    .Select(s => s.Fqdn)
                    .ToList();

                subDomainsFqdns.Add(x.Fqdn);

                return subDomainsFqdns;
            })
            .Distinct()
            .ToList();

        return dnsZones;
    }

    public static string GetSubdomainForRecord(
        string fqdn,
        string matchedDnsZone,
        string recordType,
        bool isCreate
    )
    {
        if (fqdn == matchedDnsZone && recordType == RecordTypes.A && !isCreate)
        {
            return "";
        }

        if (fqdn == matchedDnsZone && recordType == RecordTypes.A && isCreate)
        {
            return fqdn;
        }

        if (fqdn != matchedDnsZone && recordType == RecordTypes.TXT)
        {
            return fqdn;
        }

        throw new NotSupportedException(
            $"FQDN: {fqdn}, Matched Zone: {matchedDnsZone}, Record Type: {recordType} - not supported");
    }

    public static string GetDnsZoneForRecord(
        string fqdn,
        string recordType,
        IReadOnlyCollection<string> dnsZones
    )
    {
        return recordType != RecordTypes.TXT
            ? GetMatchedDnsZone(fqdn, dnsZones)
            : GetMatchedDnsZone(fqdn, dnsZones
                .Where(zone => !zone.Contains(fqdn))
                .ToList()
            );
    }

    public static string GetMatchedDnsZone(
        string fqdn,
        IReadOnlyCollection<string> dnsZones
    )
    {
        while (!string.IsNullOrWhiteSpace(fqdn))
        {
            string? zone = dnsZones
                .FirstOrDefault(zone => zone == fqdn);

            if (zone is not null)
            {
                return zone;
            }

            fqdn = Regex.Replace(fqdn, "^[^.]*\\.?", "");
        }

        throw new ArgumentException($"Unable to find a DNS Zone for {fqdn}");
    }
}
