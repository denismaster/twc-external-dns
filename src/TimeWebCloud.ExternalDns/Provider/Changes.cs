﻿namespace TimeWebCloud.ExternalDns.Provider;

public class Changes
{
    public List<Endpoint> Create { get; set; } = new();
    public List<Endpoint> UpdateOld { get; set; } = new();
    public List<Endpoint> UpdateNew { get; set; } = new();
    public List<Endpoint> Delete { get; set; } = new();
}
