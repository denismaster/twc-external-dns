using System.Text.Json;
using Serilog;
using TimeWebCloud.ExternalDns.Api;
using TimeWebCloud.ExternalDns.Provider;
using Endpoint = TimeWebCloud.ExternalDns.Provider.Endpoint;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((_, logger) =>
{
    logger.WriteTo.Console();
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<ExternalDnsProvider>();
builder.Services.AddTimeWebClient();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app
    .MapGet("/", async (ExternalDnsProvider provider) =>
        {
            var response = await provider.GetDomainsFilter();
            return TypedResults.Extensions.ExternalDnsJson(response);
        }
    )
    .WithOpenApi()
    .WithName("Negotiate")
    .WithSummary("Initialisation and negotiates headers and returns domain filter.");

app
    .MapGet(
        "/records",
        async (ExternalDnsProvider provider) =>
        {
            var response = await provider.GetCurrentRecords();
            return TypedResults.Extensions.ExternalDnsJson(response);
        }
    )
    .WithOpenApi()
    .WithName("GetRecords")
    .WithSummary("Get the current records from TimeWeb Cloud and return them.");

app
    .MapPost(
        "/records",
        async (
            Changes changes,
            ExternalDnsProvider provider,
            ILogger<Program> logger
        ) =>
        {
            logger.LogInformation(
                "Incoming changes: {data}",
                JsonSerializer.Serialize(changes)
            );

            await provider.ApplyChanges(changes);

            return TypedResults.NoContent();
        }
    )
    .WithOpenApi()
    .WithName("ChangeRecords")
    .WithSummary("Update the records in TimeWeb Cloud based on those supplied here.");

app
    .MapPost(
        "/adjustendpoints",
        async (
            ExternalDnsProvider provider,
            List<Endpoint> endpoints,
            ILogger<Program> logger
        ) =>
        {
            logger.LogInformation(
                "Incoming endpoints: {data}",
                JsonSerializer.Serialize(endpoints)
            );

            var response = await provider.AdjustEndpoints(endpoints);

            return TypedResults.Extensions.ExternalDnsJson(response);
        }
    )
    .WithOpenApi()
    .WithName("AdjustEndpoints")
    .WithSummary("Set the records in TimeWeb Cloud based on those supplied here.");

app.Run();
