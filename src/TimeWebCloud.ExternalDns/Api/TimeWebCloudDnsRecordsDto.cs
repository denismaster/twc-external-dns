﻿namespace TimeWebCloud.ExternalDns.Api;

public class TimeWebCloudDnsRecordsDto
{
    public long Id { get; set; }
    public string Type { get; set; } = string.Empty;
    public TimeWebCloudDnsRecordDataDto Data { get; set; } = new();
}
