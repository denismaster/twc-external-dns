﻿namespace TimeWebCloud.ExternalDns.Api;

public class TimeWebCloudDnsRecordDataDto
{
    public int Priority { get; set; }
    public string Value { get; set; } = string.Empty;
    public string SubDomain { get; set; } = string.Empty;
}
