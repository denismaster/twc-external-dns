﻿using Refit;

namespace TimeWebCloud.ExternalDns.Api;

public interface ITimeWebCloudApi
{
    [Get("/domains")]
    public Task<TwcGetDomainsResponse> GetAllDomains(
        [AliasAs("limit")] int limit = 100,
        [AliasAs("offset")] int offset = 0,
        CancellationToken cancellationToken = default
    );

    [Get("/domains/{fqdn}/dns-records")]
    public Task<TwcGetDnsRecordsResponse> GetDnsRecordsForFqdn(
        [AliasAs("fqdn")] string fqdn,
        [AliasAs("limit")] int limit = 100,
        [AliasAs("offset")] int offset = 0,
        CancellationToken cancellationToken = default
    );

    [Post("/domains/{fqdn}/dns-records")]
    public Task CreateDnsRecord(
        [AliasAs("fqdn")] string fqdn,
        TwcCreateDnsRecordRequest createRequest,
        CancellationToken cancellationToken = default
    );

    [Post("/domains/{fqdn}/subdomains/{subDomainFqdn}")]
    public Task CreateSubDomain(
        [AliasAs("fqdn")] string fqdn,
        [AliasAs("subDomainFqdn")] string subDomainFqdn,
        CancellationToken cancellationToken = default
    );

    [Delete("/domains/{fqdn}/dns-records/{recordId}")]
    public Task DeleteDnsRecord(
        [AliasAs("fqdn")] string fqdn,
        [AliasAs("recordId")] long recordId,
        CancellationToken cancellationToken = default
    );
}
