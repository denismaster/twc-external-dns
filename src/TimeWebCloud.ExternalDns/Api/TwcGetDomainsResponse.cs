﻿namespace TimeWebCloud.ExternalDns.Api;

public class TwcGetDomainsResponse
{
    public List<TimeWebCloudDomainDto> Domains { get; set; } = new();
}
