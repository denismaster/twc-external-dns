﻿namespace TimeWebCloud.ExternalDns.Api;

public class TimeWebCloudDomainDto
{
    public long Id { get; set; }
    public string Fqdn { get; set; } = string.Empty;
    public List<TimeWebCloudSubDomainDto> SubDomains { get; set; } = new();
}
