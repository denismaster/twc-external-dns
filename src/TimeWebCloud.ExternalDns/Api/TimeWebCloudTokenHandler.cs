﻿using System.Net.Http.Headers;
using Microsoft.Extensions.Options;

namespace TimeWebCloud.ExternalDns.Api;

public class TimeWebCloudTokenHandler: DelegatingHandler
{
    private readonly IOptions<TimeWebCloudOptions> _options;

    public TimeWebCloudTokenHandler(
        IOptions<TimeWebCloudOptions> options
    )
    {
        _options = options;
    }

    protected override async Task<HttpResponseMessage> SendAsync(
        HttpRequestMessage request, CancellationToken cancellationToken)
    {
        request.Headers.Authorization = new AuthenticationHeaderValue(
            "Bearer",
            _options.Value.Token
        );

        return await base.SendAsync(request, cancellationToken);
    }
}
