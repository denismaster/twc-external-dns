﻿using System.Text.Json.Serialization;

namespace TimeWebCloud.ExternalDns.Api;

public class TwcGetDnsRecordsResponse
{
    [JsonPropertyName("dns_records")]
    public List<TimeWebCloudDnsRecordsDto> DnsRecords { get; set; } = new();
}
