﻿using System.ComponentModel.DataAnnotations;

namespace TimeWebCloud.ExternalDns.Api;

public class TimeWebCloudOptions
{
    [Required]
    public string Token { get; set; } = string.Empty;

    [Required]
    public string ApiUrl { get; set; } = string.Empty;
}
