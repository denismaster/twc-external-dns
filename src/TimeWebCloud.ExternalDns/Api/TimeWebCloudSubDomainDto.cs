﻿namespace TimeWebCloud.ExternalDns.Api;

public class TimeWebCloudSubDomainDto
{
    public long Id { get; set; }
    public string Fqdn { get; set; } = string.Empty;
}
