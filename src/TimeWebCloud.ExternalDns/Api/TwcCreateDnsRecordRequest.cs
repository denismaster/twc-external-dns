﻿using System.Text.Json.Serialization;

namespace TimeWebCloud.ExternalDns.Api;

public class TwcCreateDnsRecordRequest
{
    public string Type { get; set; } = string.Empty;
    [JsonPropertyName("subdomain")]
    public string SubDomain { get; set; } = string.Empty;
    public string Value { get; set; } = string.Empty;
}
