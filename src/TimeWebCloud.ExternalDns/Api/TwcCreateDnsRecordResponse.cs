﻿using System.Text.Json.Serialization;

namespace TimeWebCloud.ExternalDns.Api;

public class TwcCreateDnsRecordResponse
{
    [JsonPropertyName("dns_record")]
    public TimeWebCloudDnsRecordsDto DnsRecord { get; set; } = new();
}
