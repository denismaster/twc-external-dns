﻿using Microsoft.Extensions.Options;
using Refit;

namespace TimeWebCloud.ExternalDns.Api;

public static class ServiceCollectionExtensions
{
    public static void AddTimeWebClient(this IServiceCollection services)
    {
        services.AddOptions<TimeWebCloudOptions>()
            .BindConfiguration("TimeWebCloud")
            .ValidateDataAnnotations()
            .ValidateOnStart();

        services.AddTransient<TimeWebCloudTokenHandler>();

        services.AddRefitClient<ITimeWebCloudApi>()
            .ConfigureHttpClient((serviceProvider, client) =>
            {
                var timeWebCloudOptions = serviceProvider
                    .GetRequiredService<IOptions<TimeWebCloudOptions>>();

                client.BaseAddress = new Uri(timeWebCloudOptions.Value.ApiUrl);
            })
            .AddHttpMessageHandler<TimeWebCloudTokenHandler>();
    }
}
