using System.Runtime.InteropServices;
using FluentAssertions;
using TimeWebCloud.ExternalDns.Provider;

namespace TimeWebCloud.ExternalDns.Tests;

public class ExternalDnsProviderTests
{
    [Fact]
    public void GetMatchedDnsZone_ShouldFindCorrectZone_WhenZoneNameIsEqualToInput()
    {
        // Arrange
        string zone = "test.dev.example.com";

        // Act
        string matchedZone = ExternalDnsProvider.GetMatchedDnsZone(
            zone,
            new List<string>
            {
                "test.dev.example.com",
                "dev.example.com",
                "another-domain.com"
            }
        );

        // Assert
        matchedZone.Should().Be(zone);
    }

    [Fact]
    public void GetMatchedDnsZone_ShouldFallbackToLessSpecificDomainZone()
    {
        // Arrange
        string zone = "test.dev.example.com";

        // Act
        string matchedZone = ExternalDnsProvider.GetMatchedDnsZone(
            zone,
            new List<string>
            {
                "dev.example.ru",
                "example.com",
                "another-domain.com"
            }
        );

        // Assert
        matchedZone.Should().Be("example.com");
    }

    [Fact]
    public void GetMatchedDnsZone_ShouldThrowErrors_WhenUnableToFindMatchedZone()
    {
        // Arrange
        string zone = "test.dev.example.com";

        // Act and Assert
        Assert.Throws<ArgumentException>(() =>
        {
            ExternalDnsProvider.GetMatchedDnsZone(
                zone,
                new List<string> { "dev.example.ru", "another-domain.com" }
            );
        });
    }

    [Theory]
    [InlineData("test2.example.com", RecordTypes.A,"test2.example.com")]
    [InlineData("twc-test2.example.com", RecordTypes.TXT,"example.com")]
    public void GetDnsZoneForRecord_ShouldChooseCorrectZone(
        string fqdn,
        string recordType,
        string expectedResult
    )
    {
        // Arrange
        // Act
        string actualResult = ExternalDnsProvider.GetDnsZoneForRecord(
            fqdn,
            recordType,
            new List<string>
            {
                "test2.example.com",
                "example.com",
                "another-domain.com"
            }
        );

        // Assert
        actualResult.Should().Be(expectedResult);
    }
}
